# HSBC downloader

A simple, semi-automated Ruby script to download statements from the HSBC website.

## How to use

This script requires some manual intervention. These are the steps to get it running:

1. Create a `.env` file with your personal details. See the provided `.sample.env` for reference.
2. Run this script.
3. Enter the two-factor authentication code (generated with the HSBC calculator-doohikey) when prompted on the terminal.
4. When the script-controlled Chrome instance gets to the download screen, press "Download" on the browser and manually complete the process.
