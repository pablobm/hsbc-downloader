require "webdrivers"
require "capybara"
require "selenium/webdriver"
require "dotenv/load"
require_relative "./timeutils"

def choose_account(session)
  #accounts = []
  #session.within(".dijitTitlePaneContentInner") do
    #rows = session.all(".itemDetailsContainer")
    #rows.each_with_index do |row, i|
      #account_number = row.find(".itemName").text
      #account_name = row.find(".itemTitle").text
      #puts("#{i}. #{account_number} - #{account_name}")
      #accounts << row
    #end
  #end

  #print "Target account [0-#{accounts.count-1}]:"
  #target_index = gets.chomp.to_i
  #accounts[target_index].click
  session.within(".dijitTitlePaneContentInner") do
    session.find(".itemName", text: ENV.fetch("ACCOUNT_ID")).click
  end
end

class StubbornSession < DelegateClass(Capybara::Session)
  def click_on(*args)
    begin
      super
    rescue Selenium::WebDriver::Error::UnknownError
      puts "WARNING: failed to click on #{args.first.inspect}. Trying something else..."
      if $!.message =~ %r{other element would receive the click.*id="([^"]+)"}i
        puts "Trying to remove the obscuring element"
        obscuring_element_id = $~[1]
        execute_script <<-JS
          var element = document.getElementById('#{obscuring_element_id}');
          element.remove();
        JS
        super
      else
        fail "Couldn't click on #{args.first.inspect}, much as I tried"
      end
    end
  end

  attr_reader :session
end

def get_to_download_modal(session)
  session.visit("https://www.hsbc.co.uk")
  session.click_on("Log on")
  session.fill_in("Username1", with: ENV.fetch("HSBC_USERNAME"))
  session.click_button("Continue")
  session.fill_in("memorableAnswer", with: ENV.fetch("MEMORABLE_ANSWER"))
  print "Security code: "
  session.fill_in("idv_OtpCredential", with: gets.chomp)
  session.click_button("Continue")
  choose_account(session)

  session.within(session.all(".panelInset").first) do
    session.find(".icon[title=Search]").click
    session.within(".filterDate") do
      session.fill_in("hdx_dijits_form_DateRange_1_dateFrom", with: beginning_of_last_month)
      session.fill_in("hdx_dijits_form_DateRange_1_dateTo", with: end_of_last_month)
    end
    sleep 1
    session.click_button("View results")
    session.click_button("Download results")
  end

  session.choose("QIF")
  session.click_button("Download")
end

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.register_driver :headless_chrome do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    chromeOptions: {
      args: %w(
        headless
        disable-gpu
        blink-settings=imagesEnabled=false
      )
    }
  )

  Capybara::Selenium::Driver.new(
    app,
    browser: :chrome,
    desired_capabilities: capabilities,
  )
end

Capybara.configure do |config|
  config.default_max_wait_time = 10
end

#session = Capybara::Session.new(:headless_chrome)
#session = Capybara::Session.new(:chrome)
capybara_session = Capybara::Session.new(:chrome)
session = StubbornSession.new(capybara_session)

get_to_download_modal(session)
puts("Click 'Download' on the browser to retrieve the statement. Press Enter here when done")
gets
