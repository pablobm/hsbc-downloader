def beginning_of_last_month
  month, year = last_month
  sprintf("%02d/%02d/%04d", 1, month, year)
end

def end_of_last_month
  month, year = last_month
  sprintf("%02d/%02d/%04d", days_in_month(month, year), month, year)
end

def last_month
  month = Time.now.month - 1
  if month == 0
    month = 12
  end
  year = Time.now.year
  if month == 12
    year = year - 1
  end
  [month, year]
end

COMMON_YEAR_DAYS_IN_MONTH = [nil, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

def days_in_month(month, year = Time.now.year)
   return 29 if month == 2 && Date.gregorian_leap?(year)
   COMMON_YEAR_DAYS_IN_MONTH[month]
end
